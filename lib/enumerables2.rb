require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? {|word| sub_string?(word, substring)}
end

def sub_string?(word, substring)
  word.include?(substring)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string = string.split(' ').join('')
  hash = Hash.new(0)
  string.each_char {|char| hash[char] += 1}
  hash.select {|letter, occur| occur > 1}.keys
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split(' ')
  sorted = words.sort{|a, b| a.length <=> b.length}
  [sorted[-1], sorted[-2]]
end


# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  letters = string.split('')
  alphabet = ('a'..'z').to_a
  alphabet.reject {|letter| letters.include?(letter)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select {|year| not_repeat_year?(year)}
end

def not_repeat_year?(year)
  digits = year.to_s.split('')
  return true if digits == digits.uniq
  false
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  all_songs = songs.uniq
  repeats = consecutive_songs(songs)
  all_songs.reject{|song| repeats.include?(song)}
end

def consecutive_songs(songs)
  consecutives = []
  songs.each_with_index do |song, i|
    consecutives << song if song == songs[i+1]
  end
  consecutives
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

#come back to this
def for_cs_sake(string)
  words = string.split(' ')
  c_words = words.select {|word| include_c?(word)}
  return '' if c_words.empty?
  c_words.map! { |word| only_letters(word)}
  c_words.reduce do |shortest, word|
    if c_distance(shortest) > c_distance(word)
      word
    else
      shortest
    end
  end

end

def include_c?(word)
  return true if word.downcase.include?('c')
  false
end

def c_distance(word)
  word.reverse.each_char.with_index do |char, i|
    return i if char.downcase == 'c'
  end
end

def only_letters(word)
  alphabet = ('a'..'z').to_a
  chars = word.split('')
  chars.select {|char| alphabet.include?(char.downcase)}.join('')
end

p for_cs_sake('cat inc catapult turn')

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  results = []
  i = 0
  while i < arr.length
    if arr[i] == arr[i+1]
      j = i + 1
      until j == arr.length || arr[i] != arr[j]
        j += 1
      end
      results << [i, j-1]
      i = j
    else
      i += 1
    end
  end
  results
end
p repeated_number_ranges([1, 2, 3, 3, 4, 4, 4])
p repeated_number_ranges([1, 1, 2])
